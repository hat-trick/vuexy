# Changelog

## [Unreleased]
- /

## [0.0.1] - 2022-03-13
- :rocket: Initial release

<!-- Links -->
[keep a changelog]: https://keepachangelog.com/en/1.0.0/
[semantic versioning]: https://semver.org/spec/v2.0.0.html

<!-- Versions -->
[0.0.1]: https://gitlab.com/hat-trick/vuexy/releases/tag/v0.0.1

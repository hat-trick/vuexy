# Vuexy
Vuexy theme implemented as a rails-plugin.

- Vuexy v8.0.0 jQuery
- Bootstrap 5

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'vuexy'
```

And then execute:
```bash
$ bundle
```

(Optional) Install initializer and (coming soon: locale configs)
```bash
$ rails generate vuexy:install
```

## Usage

In your controller, simply include the layout and components that you want
```ruby
class DashboardController < ApplicationController
  include Vuexy::Layouts::VerticalMenu
  include Vuexy::Components::Navigation # => Loads dashboard/navigation.yml
end
```

Configs will load from the top-level namespace of a controller:
```ruby
module Dashboard
  class ApplicationController < ::ApplicationController
    include Vuexy::Layouts::VerticalMenu
    include Vuexy::Components::Navigation # => Loads dashboard/navigation.yml
  end
end
```

Create a `navigation.yml` file in the `app/views/dashboard` directory:
```yaml
navigation:
  - header: Test Header
  - link:
      text: Dashboard
      path: <%= dashboard_root_url %>
      icon: activity
  - header: Another Header
  - link:
      text: Home
      path: <%= root_url %>
      icon: home
```

#### Layouts

- `Blank`
- `HorizontalMenu`
- `VerticalMenu`

#### Components

- `Navigation`

## License
The gem is available as open source under the terms of the [LGPL License](https://opensource.org/licenses/LGPL-3.0).

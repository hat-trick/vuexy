//= link_tree ../fonts

//= link_directory ../javascripts/vendors/js .js
//= link_directory ../javascripts/vendors/js/forms/validation .js
//= link_directory ../javascripts/vuexy .js

//= link_directory ../stylesheets/bootstrap .css
//= link_directory ../stylesheets/vendor .css
//= link_directory ../stylesheets/vuexy .css

//= link vuexy/core/app-menu.js
//= link vuexy/core/app.js

//= link vuexy/scripts/pages/auth-login.js
//= link vuexy/pages/authentication.css
//= link vuexy/plugins/forms/form-validation.css
//= link vuexy/pages/modal-create-app.css
//= link vuexy/scripts/pages/modal-create-app.js

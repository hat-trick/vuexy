# frozen_string_literal: true

module Vuexy
  module Components
    module Base
      # include Rails.application.routes.url_helpers

      def load_yaml(file:)
        YAML.safe_load(
          ERB.new(
            File.read(
              File.join(Rails.root, 'app', 'views', controller_path.split('/').first, "#{file}.yml")
            )
          ).result
        )
      end
    end
  end
end

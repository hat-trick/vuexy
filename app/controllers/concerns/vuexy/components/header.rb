# frozen_string_literal: true

module Vuexy
  module Components
    module Header
      extend ActiveSupport::Concern
      include Base

      included do
        before_action :build_header
      end

      private

      def build_header
        @vuexy_header = load_yaml(file: 'header')
      end
    end
  end
end

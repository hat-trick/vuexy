# frozen_string_literal: true

module Vuexy
  module Components
    module Navigation
      extend ActiveSupport::Concern
      include Base

      included do
        before_action :build_navigation
      end

      private

      def build_navigation
        @vuexy_navigation = load_yaml(file: 'navigation')
      end
    end
  end
end

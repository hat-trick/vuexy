# frozen_string_literal: true

module Vuexy
  module Layouts
    module Blank
      extend ActiveSupport::Concern

      included do
        layout 'vuexy/blank'
      end
    end
  end
end

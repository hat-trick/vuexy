# frozen_string_literal: true

module Vuexy
  module Layouts
    module HorizontalMenu
      extend ActiveSupport::Concern

      included do
        layout 'vuexy/horizontal-menu'
      end
    end
  end
end

# frozen_string_literal: true

module Vuexy
  module Layouts
    module VerticalMenu
      extend ActiveSupport::Concern

      included do
        layout 'vuexy/vertical-menu'
      end
    end
  end
end

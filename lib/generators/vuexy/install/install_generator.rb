# frozen_string_literal: true

module Vuexy
  class InstallGenerator < Rails::Generators::Base
    source_root File.expand_path("templates", __dir__)

    def copy_initializer_file
      copy_file 'initializer.rb', 'config/initializers/vuexy.rb'
    end
  end
end

# frozen_string_literal: true

Vuexy.configure do |c|
  c.app_name = 'App Dashboard'
  c.dark_mode = true
end

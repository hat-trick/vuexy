# frozen_string_literal: true

module Vuexy
  class Configuration < Struct.new(:app_name, :dark_mode)
    def dark_mode?
      dark_mode
    end
  end

  class << self
    def config
      @config ||= Configuration.new
    end

    def configure(&block)
      yield config
    end
  end
end

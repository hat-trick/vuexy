require 'vuexy/helpers'

module Vuexy
  class Engine < ::Rails::Engine
    isolate_namespace Vuexy

    initializer 'vuexy.helpers' do
      ActiveSupport.on_load(:action_view) do
        include Vuexy::Helpers
      end
    end
  end
end

# frozen_string_literal: true

require 'vuexy/helpers/core'
require 'vuexy/helpers/tags'
require 'vuexy/helpers/components'

module Vuexy
  module Helpers
    include Core
    include Tags
    include Components
  end
end

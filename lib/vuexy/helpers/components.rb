# frozen_string_literal: true

module Vuexy
  module Helpers
    module Components
      def vuexy_login_basic(title: nil, text: nil, &block)
        assets_for(:authentication)
        render(
          partial: 'vuexy/components/authentication/login_basic',
          locals: {
            title: title,
            text: text,
            block: block
          }
        )
      end

      def vuexy_login_cover(title: nil, text: nil, image: nil, &block)
        assets_for(:authentication)
        render(
          partial: 'vuexy/components/authentication/login_cover',
          locals: {
            title: title,
            text: text,
            image: image,
            block: block
          }
        )
      end

      def vuexy_modal(id, &block)
        render(
          partial: 'vuexy/components/modal',
          locals: {
            id: id,
            block: block
          }
        )
      end
    end
  end
end

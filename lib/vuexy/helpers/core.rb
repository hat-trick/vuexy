# frozen_string_literal: true

module Vuexy
  module Helpers
    module Core
      def assets_for(type)
        css = @vuexy_assets_css ||= ['vuexy/application']
        js = @vuexy_assets_js ||= [
          'vendors/js/vendors.min',
          'vendors/js/forms/validation/jquery.validate.min',
          'vuexy/core/app-menu',
          'vuexy/core/app',
          'vuexy/application'
        ]
        case type
        when :authentication
          css << 'vuexy/plugins/forms/form-validation'
          css << 'vuexy/pages/authentication'
          js << 'vuexy/scripts/pages/auth-login.min'
        when :calendar
          # TODO
        when :chat
          css << 'vuexy/pages/app-chat'
          css << 'vuexy/pages/app-chat-list'
          js << 'vuexy/scripts/pages/app-chat'
        when :email
          css << 'vendor/editors/quill/katex.min'
          css << 'vendor/editors/quill/monokai-sublime.min'
          css << 'vendor/editors/quill/quill.snow'
          css << 'vendor/extensions/toastr.min'
          css << 'vendor/forms/select/select2.min'
          css << 'vuexy/plugins/forms/form-quill-editor'
          css << 'vuexy/extensions/ext-component-toastr'
          css << 'vuexy/pages/app-email'
          js << 'vendors/js/editors/quill/katex.min'
          js << 'vendors/js/editors/quill/highlight.min'
          js << 'vendors/js/editors/quill/quill.min'
          js << 'vendors/js/extensions/toastr.min'
          js << 'vendors/js/forms/select/select2.full.min'
          js << 'vuexy/scripts/pages/app-email.min'
        when :kanban
          # TODO
        when :todo
          css << 'vendor/editors/quill/katex.min'
          css << 'vendor/editors/quill/monokai-sublime.min'
          css << 'vendor/editors/quill/quill.snow'
          css << 'vendor/forms/select/select2.min'
          css << 'vendor/pickers/flatpickr/flatpickr.min'
          css << 'vendor/extensions/dragula.min'
          css << 'vendor/extensions/toastr.min'
          js << 'vendors/js/editors/quill/katex.min'
          js << 'vendors/js/editors/quill/highlight.min'
          js << 'vendors/js/editors/quill/quill.min'
          js << 'vendors/js/forms/select/select2.full.min'
          js << 'vendors/js/pickers/flatpickr/flatpickr.min'
          js << 'vendors/js/extensions/dragula.min'
          js << 'vendors/js/extensions/toastr.min'
          js << 'vuexy/scripts/pages/app-todo.min'
        end
        @vuexy_assets_css = css.uniq
        @vuexy_assets_js = js.uniq
      end
    end
  end
end

# frozen_string_literal: true

module Vuexy
  module Helpers
    module Tags
      def vuexy_assets_css
        assets_for(nil) # Init
        # stylesheet_link_tag @vuexy_assets_css.join(', ')
      end

      def vuexy_assets_js
        assets_for(nil) # Init
      end

      def vuexy_icon(icon, style: nil)
        tag.i class: 'ficon', data: { feather: icon }, style: style
      end

      def vuexy_modal_close
        { bs: { dismiss: 'modal' } }
      end

      def vuexy_modal_trigger(target)
        { bs: { toggle: 'modal', target: "##{target}" } }
      end
    end
  end
end

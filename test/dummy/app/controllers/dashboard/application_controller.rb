# frozen_string_literal: true

module Dashboard
  class ApplicationController < ::ApplicationController
    include Vuexy::Layouts::VerticalMenu
    include Vuexy::Components::Header
    include Vuexy::Components::Navigation
    # include Vuexy::Components::Nick
  end
end

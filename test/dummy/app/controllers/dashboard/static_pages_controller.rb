# frozen_string_literal: true

module Dashboard
  class StaticPagesController < ApplicationController
    def index
    end
  end
end

# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :authentication do
    get 'login/basic'
    get 'login/cover'
  end
  # mount Vuexy::Engine => "/vuexy"

  namespace :dashboard, path: '/' do
    constraints subdomain: 'dashboard' do
      root to: 'static_pages#index'
    end
  end

  root to: 'static_pages#index'
end

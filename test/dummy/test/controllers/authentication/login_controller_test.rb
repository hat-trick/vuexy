require "test_helper"

class Authentication::LoginControllerTest < ActionDispatch::IntegrationTest
  test "should get basic" do
    get authentication_login_basic_url
    assert_response :success
  end
end

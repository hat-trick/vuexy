require "test_helper"

class Dashboard::StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get dashboard_static_pages_index_url
    assert_response :success
  end
end

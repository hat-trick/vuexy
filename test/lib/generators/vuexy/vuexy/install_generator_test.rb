require "test_helper"
require "generators/vuexy/install/install_generator"

module Vuexy
  class Vuexy::InstallGeneratorTest < Rails::Generators::TestCase
    tests Vuexy::InstallGenerator
    destination Rails.root.join("tmp/generators")
    setup :prepare_destination

    # test "generator runs without errors" do
    #   assert_nothing_raised do
    #     run_generator ["arguments"]
    #   end
    # end
  end
end

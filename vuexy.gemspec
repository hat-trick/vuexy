require_relative 'lib/vuexy/version'

Gem::Specification.new do |spec|
  spec.name        = 'vuexy'
  spec.version     = Vuexy::VERSION
  spec.authors     = ['Nicholaus Brabant']
  spec.email       = ['nicholaus.brabant@gmail.com']
  spec.homepage    = 'https://hattricksoftware.com/'
  spec.summary     = 'Vuexy Theme'
  spec.description = 'Vuexy Admin Theme. jQuery Version'
  spec.license     = 'GPL'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  spec.metadata['allowed_push_host'] = 'https://hattricksoftware.com/'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/hat-trick/vuexy'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/hat-trick/vuexy/CHANGELOG.md'

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']
  end

  spec.add_dependency 'haml',  '~> 5.2'
  spec.add_dependency 'rails', '>= 7.0.2'
  spec.add_dependency 'sassc', '~> 2.4'

  spec.add_development_dependency 'puma', '~> 5.6'
end
